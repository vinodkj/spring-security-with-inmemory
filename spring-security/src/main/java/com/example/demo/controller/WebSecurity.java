package com.example.demo.controller;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		auth.inMemoryAuthentication().withUser("vinod").password("letmein1").roles("ADMIN").and().withUser("shwetha")
				.password("shwetha").roles("WIFE").and().withUser("guest").password("1234").roles("VISITOR");

	}

	@Bean
	public PasswordEncoder getPasswordEndoEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/propertyroom").hasRole("ADMIN").antMatchers("/kichen").hasRole("WIFE")
				.antMatchers("/hall").hasAnyRole("ADMIN", "WIFE", "VISITOR").antMatchers("/").permitAll().and()
				.formLogin();

	}

}
